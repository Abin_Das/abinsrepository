     <!-- Page Content  -->
     <div id="content-page" class="content-page">
      <div class="container-fluid">
         <div class="row">
            <div class="col-sm-12">
                  <div class="iq-card">
                     <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                           <h4 class="card-title">User List</h4>
                        </div>
                     </div>
                     <div class="iq-card-body">
                        <div class="table-responsive">
                           <div class="row justify-content-between">
                              <div class="col-sm-12 col-md-6">
                                 <div id="user_list_datatable_info" class="dataTables_filter">
                                    <form class="mr-3 position-relative">
                                       <div class="form-group mb-0">
                                          <input type="search" class="form-control" id="exampleInputSearch" onkeyup="myFunction()" placeholder="Search" aria-controls="user-list-table">
                                       </div>
                                    </form>
                                 </div>
                              </div>
                              <div class="col-sm-12 col-md-6">
                                 <div class="user-list-files d-flex float-right">
                                  <!--    <a href="javascript:void();" class="chat-icon-phone">
                                       Print
                                     </a>
                                    <a href="javascript:void();" class="chat-icon-video">
                                       Excel
                                     </a>  -->
                                    <!--  <a href="javascript:void();" class="chat-icon-delete">
                                       Pdf
                                     </a>  -->
                                  <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Add" href="add"><i class="ri-user-add-line"></i></a> 
                                   </div>
                              </div> 
                           </div>

                           <table id="user-list-table"  class="table table-striped table-bordered mt-4" role="grid" aria-describedby="user-list-page-info">
                             <thead>
                                 <tr>
                                 <th>Sl.No</th>
                                 <th>Name</th>
                                 <th>Address</th>
                                 <th>mobile number</th>
                                 <th>email</th>
                                 <th>Username</th>
                                 <th>password</th>
                                 <th>add coin</th>
                                 <th>Action</th>
                                 </tr>
                             </thead>
                             <tbody>
                             <?php if($data): ?>
                             <?php $i=1; ?>
                             <?php foreach($data as $use): ?>
                              <tr>
                                 <td><?php echo $i ?></td>
                                 <td><?php echo $use['name']; ?></td>
                                 <td><?php echo $use['address']; ?></td>
                                 <td><?php echo $use['mobile_number']; ?></td>
                                 <td><?php echo $use['email']; ?></td>
                                 <td><?php echo $use['username']; ?></td>
                                 <td><?php echo $use['password']; ?></td>
                                 <td><?php echo $use['addcoin']; ?></td>
                                 <td>
                                       <div class="flex align-items-center list-user-action">
                                        
                                          <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="<?= base_url();?>/useredit/<?= $use['id'];?>"><i class="ri-pencil-line"></i></a>
                                          <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="<?= base_url();?>/userdelete/<?= $use['id'];?>"><i class="ri-delete-bin-line"></i></a>
                                       </div>
                                 </td>
                              </tr>
                           <?php $i++ ?>  
                           <?php endforeach; ?>
                           <?php endif; ?>
                                                                              
                             </tbody>
                           </table>
                  
                        </div>
                           <div class="row justify-content-between mt-3">
                             <!--  <div id="user-list-page-info" class="col-md-6">
                                 <span>Showing 1 to 5 of 5 entries</span>
                              </div> -->
                               <div class="col-md-6">
                                 <nav aria-label="Page navigation example">
                                    <ul class="pagination justify-content-end mb-0">
                                       <li class="page-item disabled">
                                          <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                                       </li>
                                       <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                                       <li class="page-item"><a class="page-link" href="#">3</a></li> 
                                       <li class="page-item">
                                          <a class="page-link" href="#">Next</a>
                                       </li>
                                    </ul>
                                 </nav>
                              </div> 
                            <!--   <div class="col-md-12">
                                       <div id="pagination"></div>
                              </div> -->
                           </div>
                     </div>
                  </div>
            </div>
         </div>
      </div>
   </div>

   <script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("exampleInputSearch");
  filter = input.value.toUpperCase();
  table = document.getElementById("user-list-table");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

