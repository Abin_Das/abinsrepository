<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Dashboard');
$routes->setDefaultMethod('dashboard');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Dashboard::login');
//index
$routes->get('dashboard', 'Dashboard::index');
//add_user

$routes->get('add', 'Dashboard::add_user');
//list_user
$routes->get('list', 'Dashboard::list_user');
//store_user
$routes->post('store-user', 'Dashboard::store_user');
//edit_user
// $routes->get('useredit/(:id)','Dashboard::edit');
 $routes->add('useredit/(:num)', 'Dashboard::edit/$1');
//update_user
$routes->post('update-user', 'Dashboard::update_user');
//delete_user
$routes->add('userdelete/(:num)', 'Dashboard::delete/$1');
//login
$routes->post('login', 'Dashboard::login_fn');
//log out
$routes->get('logout', 'Dashboard::signout');






/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
