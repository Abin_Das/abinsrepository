

<!-- Page Content  -->

<div id="content-page" class="content-page">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="iq-edit-list-data">
                        <div class="tab-content">
                           <div class="tab-pane fade active show" id="personal-information" role="tabpanel">
                               <div class="iq-card">
                                 <div class="iq-card-header d-flex justify-content-between">
                                    <div class="iq-header-title">
                                       <h4 class="card-title">Edit </h4>
                                    </div>
                                 </div>
                                 <div class="iq-card-body">
                                 <form action="/update-user" method="post">
                      
                                 <div class="row">
                                 <tr>
                                 <td><input type="hidden" name="id" class="form-control" id="id" value="<?php echo $data['id']; ?>"></td>
                                </tr> 
                                 <div class="form-group col-md-12">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" name="tname" disabled value="<?php echo $data['name']; ?>">
                                 <!--    <input type="text" class="form-control" name="name" id="name" autocomplete="off" placeholder="Enter customer name" value=""> -->
                                 </div>
                                 <div class="form-group col-md-12">
                                    <label for="address">Address:</label>
                                    <textarea type="textarea" class="form-control" name="taddress" id="taddress" autocomplete="off" placeholder="User Name"><?php echo $data['address']; ?></textarea> 
                                   <!--  <textarea type="textarea" class="form-control" name="address" id="address" autocomplete="off" placeholder="Enter customer address" value=""></textarea> -->
                                 </div>

                                 <div class="form-group col-md-6">
                                    <label for="mobile_number">Mobile Number:</label>
                                    <input type="text" class="form-control" name="tmobile_number" value="<?php echo $data['mobile_number']; ?>">
                                    <!-- <input type="text" class="form-control" id="mobile_number" name="mobile_number" autocomplete="off" placeholder="Enter customer mobile number" value=""> -->
                                 </div>

                                 <div class="form-group col-md-6">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" name="temail" value="<?php echo $data['email']; ?>">
                                  <!--   <input type="email" class="form-control" id="email" name="email" autocomplete="off" placeholder="Enter customer emailId" value=""> -->
                                 </div>
                                 <div class="form-group col-md-12">
                                    <label for="name">Username:</label>
                                    <input type="text" class="form-control" name="taddcoin" value="<?php echo $data['username']; ?>"> 
                                    <!-- <input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="Enter username" value=""> -->
                                 </div>
                                  <div class="form-group col-md-6">
                                    <label for="password">Password:</label>
                                    <input type="password" class="form-control" name="tpassword" value="<?php echo $data['password']; ?>"> 
                               <!--      <input type="text" class="form-control" id="password" name="password" autocomplete="off" placeholder="Password" value=""> -->
                                 </div> 
                                    <div class="form-group col-md-6">
                                    <label for="addcoin">Add Coins:</label>
                                    <input type="text" class="form-control" name="taddcoin" value="<?php echo $data['addcoin']; ?>"> 
                                <!--     <input type="text" class="form-control" id="addcoin" name="addcoin" autocomplete="off" placeholder="Add coins" value=""> -->
                                 </div> 
                                 <?php if(isset($validation)): ?>
                                    <div class="col-12">
                                       <div class="alert alert-danger" role="alert">
                                          <?= $validation->listErrors() ?>
                                       </div>
                                    </div>
                                 <?php endif; ?>
  
                              </div>
                        
                              <button type="submit" class="btn btn-primary">Edit</button>
                           </form>
                                 </div>
                              </div>
                           </div>
                                               
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
