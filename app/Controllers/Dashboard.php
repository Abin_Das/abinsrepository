<?php namespace App\Controllers;

use App\Models\UserModel;

class Dashboard extends BaseController
{
    
    public function login()
    {
        echo view('auth/login');
      
    }
   /*  public function login_fn()
    {
    //  dd('hi');
        if($this->exists($_POST['username'],$_POST['password']!= NULL))
        {
           // dd('hi');
            $session=session();
            $session->set('username',$_POST['admin']);
            return $this->response->redirect(site_url('Dashborad/index'));
        }
        else{
            $test['msg']="invalid";
            return view('login',$test);
        }
    }
    public function exists($username,$password)
    {
        $model = new UserModel();
        $data = $model->where('username',$username)->first();
        if($data != NULL)
        {
            if(password_verify($password,$data['password']))
            {
                return $data;
            }
        }
    } */
     public function login_fn()
    {
    //  dd('hi');
        $username=$this->request->getVar('username');
        $password=$this->request->getVar('password');
       
      // dd($username);
        $model = new UserModel();
        $array = array('username' => $username, 'password' => $password , 'role' => 1);
       // dd($array);
        $data = $model->where($array)->first();
        if($data != NULL)
        {
         //   $session = \Config\Services::session($config);
            $session=session();
            $session->set(['username'=>$username]);
            return $this->response->redirect('dashboard');
          //$s=  $session->get('username');
            //echo $s;
        } 
        else{
            $session=session();
            $session->setFlashdata('danger','Invalid Username and Password');
            return $this->response->redirect('/');
        }
    }
    public function index()
    {

      
        echo view('includes/header');
        echo view('dashboard/index');
        echo view('includes/footer');
        
    }
    public function add_user()
    {
        


        echo view('includes/header');
        echo view('dashboard/add_user');
        echo view('includes/footer');
    
          
    }
    public function store_user()
    {
        
        $data = [];
        helper(['form']);
 
        if($this->request->getMethod() == 'post')
        {
            //dd("hi");
            $rules=[
                'name'=> 'required|min_length[3]|max_length[30]',
                'address' =>'required',
                'mobile_number' =>'required|min_length[10]|max_length[10]',
                'username'=>'required|min_length[4]|max_length[20]|is_unique[adduser.username]',
                'email'=>'required|valid_email',
                'password'=>'required|min_length[6]|max_length[50]',
            ];
          
            if(! $this->validate($rules))
                {
                    $data['validation'] = $this->validator;
                    //dd($data);
                }
            else{
                
                    $model = new UserModel();
                            
                    // dd($this->request->getVar('address'));          
                    $newData = [
                        'name' => $this->request->getVar('name'),
                        'address' => $this->request->getVar('address'),
                        'mobile_number' => $this->request->getVar('mobile_number'),
                        'username' => $this->request->getVar('username'),
                        'email' => $this->request->getVar('email'),
                        'password' => $this->request->getVar('password'),
                        'addcoin' => $this->request->getVar('addcoin'), 
                        'role' => 2, 
                    ];
            }
           
     
      
      
        $model->insert($newData);
        $session = session();
        $session->setFlashdata('success','Successful registration');
        return redirect()->to('list');
       
    }

    }

    public function list_user()
    {
       // $pager = \Config\Services::pager();

        $model = new UserModel();
    
        // $array = ['username' =>'user'];
         $data = $model->where('role',2)->findAll();
    
       /*    $data1 = [
            'UserModel' => $data->paginate(1),
            'pager' => $data->pager
        ];   */
        // $apple = $data['adduser'];
        // dd($apple);

      //  return view('dashboard\list_user', ['apple'=>$apple]);
 
        echo view('includes/header');
        echo view('dashboard/list_user',['data'=>$data]);
        echo view('includes/footer'); 
    
    }
    
    public function edit($id = "null")
    {
    //  echo $id;die;
     $model = new UserModel();
        
     $data = $model->find($id);
     
    // dd($id);
//  dd($data);
     
   //  echo view('includes/header');
     echo view('dashboard/edit_user',['data'=>$data]);
   //  echo view('includes/footer'); 
 
    /*  return view('edit', ['data'=>$data]); */
    }
     public function update_user()
    {
        

        helper(['form']);
 
        if($this->request->getMethod() == 'post')
        {
            //dd("hi");
     
        $model = new UserModel();
        $id = $this->request->getVar('id');
      //   $data1 = $model->find($id);
       //  dd($data1);
       //dd($id);
        
        $data = [
        'name' => $this->request->getVar('tname'),
        'address'  => $this->request->getVar('taddress'),
        'mobile_number'  => $this->request->getVar('tmobile_number'),
        'email'  => $this->request->getVar('temail'),
        'username'  => $this->request->getVar('tusername'),
        'password'  => $this->request->getVar('tpassword'),
        'addcoin'  => $this->request->getVar('taddcoin'),

        ];
        //dd($data);
         $model ->update($id,$data);
       // dd($save);
        return redirect()->to('/list');

       
    }

    } 
    public function delete($id = null){
        helper(['form']);
		$model = new UserModel();
		$data['adduser'] = $model->where('id', $id)->delete(); 
        return redirect()->to( base_url('list') );
        //return redirect('list');
    }


	//--------------------------------------------------------------------
 public function signout()
{
    // dd("hi");
   
    $session=session();
   
    $s=  $session->get('username');
    //dd($s);
    $session->destroy('username');
    // dd("hi");
    // $session->set(['username'=>$username]);
    return $this->response->redirect('/');
} 
}

