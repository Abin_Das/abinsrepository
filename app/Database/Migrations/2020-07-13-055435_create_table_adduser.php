<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateTableAdduser extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
					'type'           => 'INT',
					'constraint'     => 5,
					'unsigned'       => TRUE,
					'auto_increment' => TRUE
			],
			'name'       => [
					'type'           => 'VARCHAR',
					'constraint'     => '100',
			],
			'username'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'address' => [
					'type'           => 'TEXT',
					'null'           => FALSE,
			],
			'mobile_number'          => [
				'type'           => 'BIGINT',
				'null'           => FALSE,
			],
			'email'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'password'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
			'addcoin'          => [
				'type'           => 'INT',
				'null'           => TRUE,
				
			],
			'role'          => [
				'type'           => 'INT',
				'null'			 => False,
				
			],
		
	]);
	$this->forge->addKey('id', TRUE);
	$this->forge->createTable('adduser');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->droptable('adduser');
	}
}
