<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'adduser';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['name','username','address','mobile_number','email','password','addcoin','role'];

    protected $useTimestamps = false;
    /* protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at'; */

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

}