<?php 
namespace App\Validation;
use App\Models\UserModel;   

class UserRules 
{
    public function val(string $str, string $fields, array $data)
    {
        // dd("hi");
        $model = new UserModel();
        // dd($model->USERNAME);
        $user = $model->where('username', $data['username'])
                      ->first();
                    //   dd($user);
        if(!$user)
            return false;
        return password_verify($data['password'], $user['password']);
    }
}