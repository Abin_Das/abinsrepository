<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Codeigniter 4 Pagination Example - Tutsmake.com</title>
  <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> -->
</head>
<body>
<div class="container">
 
   <div class="container">
    <div class="row mt-5">
       <table class="table table-bordered">
         <thead>
            <tr>
               <th>Id</th>
               <th>Name</th>
               <th>Address</th>
               <th>mobile number</th>
               <th>email</th>
               <th>password</th>
               <th>add coin</th>
            </tr>
         </thead>
         <tbody>
            <?php if($apple): ?>
            <?php foreach($apple as $use): ?>
            <tr>
               <td><?php echo $use['id']; ?></td>
               <td><?php echo $use['name']; ?></td>
               <td><?php echo $use['address']; ?></td>
               <td><?php echo $use['mobile_number']; ?></td>
               <td><?php echo $use['email']; ?></td>
               <td><?php echo $use['password']; ?></td>
               <td><?php echo $use['addcoin']; ?></td>
            </tr>
           <?php endforeach; ?>
           <?php endif; ?>
         </tbody>
       </table>
     
    </div>
  </div>
</div> 
</body>
</html>