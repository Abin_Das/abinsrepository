
<?php   
$session=session();
$s=isset($_SESSION['username']);
if($s != 1)
{
   return $this->response->redirect('/');
}
?>
  
  
  
  <div id="content-page" class="content-page">
      <div class="container-fluid">
         <div class="row">
          <!--   <div class="col-lg-3">
                  <div class="iq-card">
                     <div class="iq-card-header d-flex justify-content-between">
                     
                     </div>
                     <div class="iq-card-body">
                        <form>
                           <div class="form-group">
                              <div class="add-img-user profile-img-edit">
                                 <img class="profile-pic img-fluid" src="images/user/icon.jpg" alt="profile-pic">
                                 <div class="p-image">
                                   <a href="javascript:void();" class="upload-button btn iq-bg-primary">File Upload</a>
                                   <input class="file-upload" type="file" accept="image/*">
                                </div>
                              </div> 
                             <div class="img-extension mt-3">
                                <div class="d-inline-block align-items-center">
                                    <span>Only</span>
                                   <a href="javascript:void();">.jpg</a>
                                   <a href="javascript:void();">.png</a>
                                   <a href="javascript:void();">.jpeg</a>
                                   <span>allowed</span>
                                </div>
                             </div> 
                           </div>
                        
                        </form>
                     </div>
                  </div>
            </div> -->
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                  <div class="iq-card">
                     <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                           <h4 class="card-title">Add New Customer</h4>
                        </div>
                     </div>

                     <div class="iq-card-body">
                        <div class="new-user-info">
                           <form class="" action="store-user" method="post">
                              <div class="row">
                                
                                 <div class="form-group col-md-12">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" name="name" id="name" autocomplete="off" placeholder="Enter customer name" value="">
                                 </div>
                                 <div class="form-group col-md-12">
                                    <label for="address">Address:</label>
                                    <textarea type="textarea" class="form-control" name="address" id="address" autocomplete="off" placeholder="Enter customer address" value=""></textarea>
                                 </div>

                                 <div class="form-group col-md-6">
                                    <label for="mobile_number">Mobile Number:</label>
                                    <input type="tel" class="form-control" id="mobile_number" name="mobile_number" autocomplete="off" placeholder="Enter customer mobile number" value="">
                                 </div>

                                 <div class="form-group col-md-6">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email" autocomplete="off" placeholder="Enter customer emailId" value="">
                                 </div>
                                 <div class="form-group col-md-12">
                                    <label for="name">Username:</label>
                                    <input type="text" class="form-control" name="username" id="username" autocomplete="off" placeholder="Enter username" value="">
                                 </div>
                                  <div class="form-group col-md-6">
                                    <label for="password">Password:</label>
                                    <input type="password" class="form-control" id="password" name="password" autocomplete="off" placeholder="Password" value="">
                                 </div> 
                                    <div class="form-group col-md-6">
                                    <label for="addcoin">Add Coins:</label>
                                    <input type="text" class="form-control" id="addcoin" name="addcoin" autocomplete="off" placeholder="Add coins" value="">
                                 </div> 
                                 <?php if(isset($validation)): ?>
                                    <div class="col-12">
                                       <div class="alert alert-danger" role="alert">
                                          <?= $validation->listErrors() ?>
                                       </div>
                                    </div>
                                 <?php endif; ?>
  
                              </div>
                              <button type="submit" class="btn btn-primary" style="background-color:#257337;">Add New Customer</button>
                             
                             
                           </form>
                        </div>
                     </div>
                  </div>
            </div>
         </div>
      </div>
   </div>
